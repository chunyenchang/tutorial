<?php
define('BASEURL', $_SERVER['DOCUMENT_ROOT'].'/tutorial/');
define('CART_COOKIE','SBwi72UCklwiqzz2');
define('CART_COOKIE_EXPIRE',time() + (86400 *30));
define('TAXRATE',0.05); //Sales tax rate. Set to 0 if you are arn't charging tax

define('CURRENCY', 'usd');
define('CHECKOUTMODE','TEST'); //Change TEST to LIVE when you are ready to go LIVE

if(CHECKOUTMODE == 'TEST'){
    define('STRIPE_PRIVATE','pk_test_FCePkH6isxcCLoakIxTvw8xK00e61SjCEn');
    define('STRIPE_PUBLIC', 'sk_test_b51go3sAhmnacdfUyt74rvCV00YKIxm9f9');
}

if(CHECKOUTMODE == 'LIVE'){
    define('STRIPE_PRIVATE','');
    define('STRIPE_PUBLIC', '');
}